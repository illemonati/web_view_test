#![windows_subsystem = "windows"]

extern crate web_view;

use web_view::*;
use std::process;

fn main() {

    let main_css = include_str!("www/main.css");
    let main_js = include_str!("www/main.js");
    let index_html = format!{r#"
    <!doctype html>
    <html>
        <head>
            <style>
                {css}
            </style>
            <script type="text/javascript">
                {js}
            </script>
        <head>
        <body>
            <center>
            <h1>Web View Test</h1>
            <button onclick="external.invoke('open')">Open</button>
            <button onclick="external.invoke('exit')">Exit</button>
            </center>
        </body>
    </html>
    "#,
    css = main_css,
    js = main_js
    };

    let webview = web_view::builder()
        .title("Mac Rust Test")
        .content(Content::Html(index_html))
        .size(800, 600)
        .resizable(true)
        .debug(true)
        .user_data(())
        .invoke_handler(|webview, arg| {
            match arg {
                "open" => match webview.dialog().open_file("Please choose a file...", "")? {
                    Some(path) => webview.dialog().info("File chosen", path.to_string_lossy()),
                    None => webview
                        .dialog()
                        .warning("Warning", "You didn't choose a file."),
                }?,
                "exit" => {
                    process::exit(1);
                }
                _ => unimplemented!(),
            };
            Ok(())
        })
        .build().unwrap();

        webview.run();
}
